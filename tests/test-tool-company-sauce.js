var assert = require('assert'),
    fs = require('fs'),
    test = require('selenium-webdriver/testing'),
    webdriver = require('selenium-webdriver');
var sauce = 'http://ondemand.saucelabs.com:80/wd/hub';

test.describe('Tool Create Company', function(){
    this.timeout(15000);
    var driver;

    test.before(function(){
        driver = new webdriver.Builder().
            usingServer(sauce).
            withCapabilities({
                browserName: 'Chrome',
                name: 'Sample selenium-webdriver test',
                username: 'tony_pixelant',
                accessKey: 'df60b367-b5e1-426a-a739-190844ad27c6'
            }).
            build();
    });

    test.it('Should be able to create new company', function(){
        driver.get('http://cliff:cliff@resultify_qa.pixelant.nu/login');
        driver.findElement(webdriver.By.id('username')).sendKeys('tony');
        driver.findElement(webdriver.By.id('password')).sendKeys('tony');
        driver.findElement(webdriver.By.id('form_login')).submit();

        driver.get('http://resultify_qa.pixelant.nu/company');
        driver.findElement(webdriver.By.className('btn')).click();

        driver.findElement(webdriver.By.name('newCompany[name]')).sendKeys('New Company');
        driver.findElement(webdriver.By.name('newCompany[Address1]')).sendKeys('New Company Address Line 1');
        driver.findElement(webdriver.By.name('newCompany[Address2]')).sendKeys('New Company Address Line 2');
        driver.findElement(webdriver.By.name('newCompany[zip]')).sendKeys('79000');
        driver.findElement(webdriver.By.name('newCompany[city]')).sendKeys('City1');
        driver.findElement(webdriver.By.name('newCompany[country]')).sendKeys('Country1');
        driver.findElement(webdriver.By.name('newCompany[state]')).sendKeys('State1');
        driver.findElement(webdriver.By.name('newCompany[phone]')).sendKeys('+3009999999');
        driver.findElement(webdriver.By.name('newCompany')).submit();

        driver.wait(function(){
            return driver.isElementPresent(webdriver.By.className('sidebar-menu'));
        }, 3000);
    });

    test.after(function(){
        driver.quit();
    });
});