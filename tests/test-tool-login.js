var assert = require('assert'),
    fs = require('fs'),
    test = require('selenium-webdriver/testing'),
    webdriver = require('selenium-webdriver');


test.describe('Tool Login', function(){
    this.timeout(15000);
    var driver;

    test.before(function(){
        driver = new webdriver.Builder().
            withCapabilities(webdriver.Capabilities.chrome()).
            build();
    });

    test.it('Login form should work', function(){
        driver.get('http://cliff:cliff@resultify_qa.pixelant.nu/login');
        driver.findElement(webdriver.By.id('username')).sendKeys('tony');
        driver.findElement(webdriver.By.id('password')).sendKeys('tony');
        driver.findElement(webdriver.By.id('form_login')).submit();

        driver.wait(function(){
            return driver.isElementPresent(webdriver.By.className('sidebar-menu'));
        }, 3000);
    });

    test.after(function(){
        driver.quit();
    });
});